> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# lis4368

## Natalie Bosso

### Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Create Git Repository	

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL
    - Develop and Deploy a WebApp
    - Debug	

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create ERD's using MySQL 
	- Forward Engineering
	- Debugging
	- Uploading MySQL files to repository

4.  [P1 README.md](p1/README.md "My P1 README.md file")
    - Data Validation
    - Use of Java/CSS/HTML5
    - Debug
    - Upload files to repository

5.  [A4 README.md](a4/README.md "My A4 README.md file")
    - Data Validation
    - Use of Java/CSS/HTML5
    - Debug
    - Upload files to repository

6.  [A5 README.md](a5/README.md "My A5 README.md file")
    - Data Validation
    - Use of Java/MySQL
    - Debug
    - Upload files to repository

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Data Validation
    - Use of Java/MySQL
    - Debug
    - Upload files to repository
	