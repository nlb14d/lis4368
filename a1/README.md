> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Natalie Bosso

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost:9999
* git commands w/ short descriptions
* Bitbucket repo links a)this assignment and b) the completed tutorial repos above

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: creates empty Git repository
2. git status: checks current state of the Git repository
3. git add: updates the index
4. git commit: records changes to repository
5. git push: updates remote refs along with associated objects
6. git pull: fetch from and integrate with another repository or local branch
7. git config: sets config. values for your username, email, file formats, etc.

#### Assignment Screenshots:

*Screenshot of Tomcat Installation:
![Tomcat Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello:
![java Hello Screenshot](img/java_Hello.png)

*Screenshot of JDK Installation:
![JDK Installation Screenshot](img/jdk_install.png)


#### Tutorial Links:

*Bitbucket https repo link: https://nlb14d@bitbucket.org/nlb14d/lis4368*
*Bitbucket ssh repo link: git@bitbucket.org:nlb14d/lis4368*
*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
