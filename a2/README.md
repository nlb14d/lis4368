> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Natalie Bosso

### Assignment 2 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. MySQL/WebServlet Development Installation
3. Chapter Questions (Chs 5, 6)

#### README.md file should include the following items:

*Screenshot of http://localhost:9999/hello
*Screenshot of http://localhost:9999/index.html
*Screenshot of http://localhost:9999/sayhello
*Screenshot of http://localhost:9999/sayhi
*Screenshot of http://localhost:9999/querybook.html
*Screenshot of http://localhost:9999/hello/query?author=Tan+Ah+Teck

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> 
#### Assignment Screenshots:

[http://localhost:9999/hello](http://localhost:9999/hello):
![hello Installation Screenshot](img/hello.png)

[http://localhost:9999/index.html](http://localhost:9999/index.html):
![hello Screenshot](img/hello.png)

[http://localhost:9999/sayhello](http://localhost:9999/sayhello):
![sayhelllo Screenshot](img/sayhello.png)

[http://localhost:9999/sayhi](http://localhost:9999/sayhi):
![sayhi Screenshot](img/sayhi.png)

[http://localhost:9999/querybook.html](http://localhost:9999/querybook.html):
![querybook Screenshot](img/querybook.png)

[http://localhost:9999/hello/query?author=Tan+Ah+Teck](http://localhost:9999/hello/query?author=Tan+Ah+Teck):
![querybook Screenshot](img/queryresults.png)


#### Tutorial Links:

*Bitbucket https repo link: https://nlb14d@bitbucket.org/nlb14d/lis4368*
*Bitbucket ssh repo link: git@bitbucket.org:nlb14d/lis4368*
*Tutorial: Request to update a teammate's repository:*

