> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Natalie Bosso

### Assignment 3 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. MySQL Program and Language
3. Chapter Questions (Chs 7, 8)

#### README.md file should include the following items:

*Screenshot of ERD
*Links to .mwb and .sql


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> 
#### Assignment Screenshots:

*Screenshot of ERD:
![ERD Screenshot](img/erd.png)

* Link to .mwb: [a3.mwb file](a3.mwb)


* Link to .sql: [a3.sql file](a3.sql)


#### Tutorial Links:

*Bitbucket https repo link: https://nlb14d@bitbucket.org/nlb14d/lis4368*
*Bitbucket ssh repo link: git@bitbucket.org:nlb14d/lis4368*
*Tutorial: Request to update a teammate's repository:*

