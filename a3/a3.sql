-- MySQL Script generated by MySQL Workbench
-- Wed Feb 22 15:33:22 2017
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema nlb14d
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `nlb14d` ;

-- -----------------------------------------------------
-- Schema nlb14d
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `nlb14d` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `nlb14d` ;

-- -----------------------------------------------------
-- Table `nlb14d`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nlb14d`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `nlb14d`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) NOT NULL,
  `pst_phone` BIGINT NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sale` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `nlb14d`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nlb14d`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `nlb14d`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT NOT NULL,
  `cus_phone` BIGINT NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `nlb14d`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nlb14d`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `nlb14d`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NOT NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `pst_id_idx` (`pst_id` ASC),
  INDEX `cus_id_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pst_id`
    FOREIGN KEY (`pst_id`)
    REFERENCES `nlb14d`.`petstore` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cus_id`
    FOREIGN KEY (`cus_id`)
    REFERENCES `nlb14d`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `nlb14d`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `nlb14d`;
INSERT INTO `nlb14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sale`, `pst_notes`) VALUES (1, 'PetSmart', '420 Apalachee Parkway', 'Tallahassee', 'FL', 32304, 8501234567, 'contact@gmail.com', 'petsmart.com', 2017-01-10, NULL);
INSERT INTO `nlb14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sale`, `pst_notes`) VALUES (2, 'Petz', '666 W Call Street', 'Tallahassee', 'FL', 32304, 8502345678, 'emailus@gmail.com', 'petz.com', 2017-01-01, NULL);
INSERT INTO `nlb14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sale`, `pst_notes`) VALUES (3, 'Dogs', '1111 Mountain Drive', 'Tallahassee', 'FL', 32304, 8503456789, 'dog@gmail.com', 'dogs.com', 2017-01-02, NULL);
INSERT INTO `nlb14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sale`, `pst_notes`) VALUES (4, 'Cats', '1738 Ocean View Road', 'Tallahassee', 'FL', 44569, 8504567890, 'meow@gmail.com', 'cats.com', 2017-01-03, NULL);
INSERT INTO `nlb14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sale`, `pst_notes`) VALUES (5, 'Pet Store', '2414 4th Avenue', 'Tallahassee', 'FL', 32304, 8505678901, 'petstore@gmail.com', 'petstore.com', 2017-01-04, NULL);
INSERT INTO `nlb14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sale`, `pst_notes`) VALUES (6, 'Dogs and More', '200 N Monroe St', 'Tallahassee ', 'FL', 32301, 8507890123, 'dogsandmore@gmail.com', 'dogsandmore.com', 2017-01-05, NULL);
INSERT INTO `nlb14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sale`, `pst_notes`) VALUES (7, 'Animalz', '100 N Monroe St', 'Tallahassee', 'FL', 32301, 8508901234, 'animalz@gmail.com', 'animalz.com', 2017-01-06, NULL);
INSERT INTO `nlb14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sale`, `pst_notes`) VALUES (8, 'Horses', '100 Call St', 'Tallahassee', 'FL', 32301, 8509012345, 'horses@gmail.com', 'horses.com', 2017-01-07, NULL);
INSERT INTO `nlb14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sale`, `pst_notes`) VALUES (9, 'Small Pets', '20 Tennessee St', 'Orlando', 'FL', 32394, 8501123456, 'smallpets@gmail.com', 'smallpets.com', 2017-01-08, NULL);
INSERT INTO `nlb14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sale`, `pst_notes`) VALUES (10, 'Rodentz', '10 Call St', 'Tallahassee', 'FL', 32304, 8501223456, 'rodentz@gmail.com', 'rodentz.com', 2017-01-09, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `nlb14d`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `nlb14d`;
INSERT INTO `nlb14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (10, 'Natalie', 'Bosso', '100 Florida St', 'Tallahassee', 'FL', 32304, 5611234567, 'bosso@gmail.com', 100.00, 50.00, NULL);
INSERT INTO `nlb14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (9, 'Fruit', 'Apple', '200 Florida St', 'Tallahassee', 'FL', 32302, 5612345678, 'apple@gmail.com', 200.00, 51.00, NULL);
INSERT INTO `nlb14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (8, 'Kelly', 'Orange', '300 Florida St', 'Orlando', 'FL', 32301, 5613456789, 'orange@gmail.com', 110.00, 55.00, NULL);
INSERT INTO `nlb14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (7, 'Harold', 'Melon', '400 Florida St', 'Tallahassee', 'FL', 32303, 5614567890, 'melon@gmail.com', 111.00, 56.00, NULL);
INSERT INTO `nlb14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (6, 'Colby', 'Berry', '500 Florida St', 'Tallahassee', 'FL', 32305, 5615678901, 'berry@gmail.com', 112.00, 57.00, NULL);
INSERT INTO `nlb14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (5, 'Sarah', 'Tree', '600 Florida St', 'Tallahassee', 'FL', 32306, 5616789012, 'tree@gmail.com', 113.00, 58.00, NULL);
INSERT INTO `nlb14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (4, 'Melissa', 'Flower', '700 Florida St', 'Orlando', 'FL', 32307, 5617890123, 'flower@gmail.com', 114.00, 59.00, NULL);
INSERT INTO `nlb14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (3, 'Jackie', 'Thorn', '800 Florida St', 'Tallahassee', 'FL', 32308, 5619012345, 'thorn@gmail.com', 115.00, 60.00, NULL);
INSERT INTO `nlb14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (2, 'Ricky', 'Rose', '900 Florida St', 'Tallahassee', 'FL', 32309, 5611123456, 'rose@gmail.com', 116.00, 70.00, NULL);
INSERT INTO `nlb14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (1, 'Lucy', 'Daisy', '1 Georgia St', 'Tallahassee', 'FL', 32311, 5612234567, 'daisy@gmail.com', 117.00, 80.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `nlb14d`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `nlb14d`;
INSERT INTO `nlb14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (11, 1, 10, 'Dog', 'm', 120.00, 300.00, 1, 'Brown', NULL, 'y', 'y', NULL);
INSERT INTO `nlb14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (12, 2, 9, 'Cat', 'm', 56.00, 100.00, 1, 'White', '2012-01-21', 'y', 'y', NULL);
INSERT INTO `nlb14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (13, 3, 8, 'Snake', 'f', 32.20, 50.00, 1, 'Black', NULL, 'y', 'n', NULL);
INSERT INTO `nlb14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (14, 4, 7, 'Hamster', 'f', 30.00, 5.00, 2, 'Brown', '2014-02-14', 'y', 'n', NULL);
INSERT INTO `nlb14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (15, 5, 6, 'Bird', 'm', 15.00, 20.00, 2, 'Orange', NULL, 'n', 'n', NULL);
INSERT INTO `nlb14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (16, 6, 5, 'Mouse', 'm', 20.00, 30.00, 1, 'White', NULL, 'y', 'n', NULL);
INSERT INTO `nlb14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (17, 7, 4, 'Rabbit', 'f', 50.00, 60.00, 1, 'Black', NULL, 'y', 'n', NULL);
INSERT INTO `nlb14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (18, 8, 3, 'Horse', 'f', 500.00, 600.00, 2, 'Paint', NULL, 'y', 'y', NULL);
INSERT INTO `nlb14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (19, 9, 2, 'Bird', 'f', 30.00, 40.00, 3, 'Yellow', NULL, 'y', 'y', NULL);
INSERT INTO `nlb14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (20, 10, 1, 'Hamster', 'm', 20.00, 25.00, 1, 'Blonde', NULL, 'y', 'n', NULL);

COMMIT;

