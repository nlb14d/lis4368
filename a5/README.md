> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Natalie Bosso

### Assignment 5 Requirements:

*Sub-Heading:*

1. Course Title, name, assignment requirements
2. Screenshots
3. Ch Questions (13, 14)


#### README.md file should include the following items:

* Screenshot of valid user entry
* Screenshot of passed validation
* Screenshot of database


> This is a blockquote.
>
> This is the second paragraph in the blockquote.


##### Assignment Screenshots:


* Screenshot of valid user entry;

![ERD Screenshot](img/valid.png)

* Screenshot of passed validation;

![ERD Screenshot](img/success.png)

* Screenshot of database;

![ERD Screenshot](img/db.png)