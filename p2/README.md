> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Natalie Bosso

### Project 2 Requirements:

*Sub-Heading:*

1. Course Title, name, assignment requirements
2. Screenshots
3. Ch Questions (16, 17)


#### README.md file should include the following items:

* Screenshot of valid user entry
* Screenshot of passed validation
* Screenshot of display data
* Screenshot of modify form
* Screenshot of modified data
* Screenshot of delete warning
* Screenshot of associated database changes



> This is a blockquote.
>
> This is the second paragraph in the blockquote.


##### Assignment Screenshots:


* Screenshot of valid user entry:

![Valid Screenshot](img/valid2.png)

* Screenshot of passed validation:

![Passed Screenshot](img/passed2.png)

* Screenshot of display data:

![Display Screenshot](img/display.png)

* Screenshot of modify form:

![Form Screenshot](img/form.png)

* Screenshot of modified data:

![Data Screenshot](img/data.png)

* Screenshot of delete warning:

![Warning Screenshot](img/warning.png)

* Screenshot of associated database changes:

![Database Screenshot](img/database.png)












